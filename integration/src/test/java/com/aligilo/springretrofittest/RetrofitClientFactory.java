package com.aligilo.springretrofittest;

import lombok.RequiredArgsConstructor;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RequiredArgsConstructor
public class RetrofitClientFactory {
    private final int port;

    PersonAPIClient authenticatedClient(String username, String password) {
        //Springboottest runs under port 8085, as specified in src/test/resources/application.properties
        OkHttpClient okHttpClient = new OkHttpClient.Builder().authenticator(
                (route, response) -> response.request().newBuilder().header("Authorization", Credentials.basic(username, password))
                        .build()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(String.format("http://localhost:%d/", port)).
                        addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(PersonAPIClient.class);
    }
}
