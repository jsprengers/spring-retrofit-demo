package com.aligilo.springretrofittest;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy;
import org.testcontainers.utility.DockerImageName;

public class AppContainer extends GenericContainer<AppContainer> {

    public AppContainer() {
        //The dockerized springboot app run on port 8080, the only port that is exposed by the image
        super(DockerImageName.parse("spring-retrofit-test-server:latest"));
        withExposedPorts(8080);
    }

    protected void startAndWait(){
        this.start();
        //The container port 8080 is mapped to a free port on the host, available through getFirstMappedPort()
        this.waitingFor(new HttpWaitStrategy().forPath("api/person/").forPort(getFirstMappedPort()));
    }
}
