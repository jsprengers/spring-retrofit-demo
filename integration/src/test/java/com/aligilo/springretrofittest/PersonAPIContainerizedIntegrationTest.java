package com.aligilo.springretrofittest;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class PersonAPIContainerizedIntegrationTest {

    private static AppContainer container;
    private static PersonAPIClient userClient;
    private static PersonAPIClient adminClient;

    @BeforeAll
    public static void initialize() {
        container = new AppContainer();
        container.startAndWait();
        //The port for the localhost endpoint is available through container.getFirstMappedPort()
        RetrofitClientFactory retrofitClientFactory = new RetrofitClientFactory(container.getFirstMappedPort());
        userClient = retrofitClientFactory.authenticatedClient("user","nosecret");
        adminClient = retrofitClientFactory.authenticatedClient("admin", "secret");
    }

    @AfterAll public static void shutdown() {
        if (container != null && container.isRunning())
            container.stop();
    }

    @Test
    public void EntityLifeCycleHappyFlow() throws IOException {
        executeCall(adminClient.createPerson(Person.builder().id("42").name("Jane").build()));
        executeCall(adminClient.createPerson(Person.builder().id("43").name("Jack").build()));
        assertThat(executeCall(userClient.getPersonById("42", null)).body().getName()).isEqualTo("Jane");
        assertThat(executeCall(userClient.getPersonById("43", null)).body().getName()).isEqualTo("Jack");

        Response<List<Person>> response = executeCall(userClient.getAll(null));
        assertThat(response.body()).hasSize(2);

        executeCall(adminClient.upsertPerson(Person.builder().id("42").name("Jane").address("London").build()));
        Person jane = executeCall(userClient.getPersonById("42", "address,dateofBirth")).body();
        assertThat(jane.getAddress()).isEqualTo("London");
        executeCall(adminClient.deletePerson("42"));
        assertThat(userClient.getAll(null).execute().body()).hasSize(1);

    }

    @Test
    public void testAuthorizationsForUserRole() throws IOException {
        assertThat(userClient.getAll(null).execute().code()).isEqualTo(200);
        assertThat(userClient.createPerson(Person.builder().id("42").name("Jane").build()).execute().code()).isEqualTo(403);
        assertThat(userClient.upsertPerson(Person.builder().id("42").name("Jane").build()).execute().code()).isEqualTo(403);
        assertThat(userClient.deletePerson("42").execute().code()).isEqualTo(403);
    }

    @Test
    public void retrieveNonExistinguserIdReturnsNOT_FOUND() throws IOException {
        //unhappy flow scenario: unknown ID
        Response<Person> noSuchPerson = userClient.getPersonById("45", null).execute();
        assertThat(noSuchPerson.code()).isEqualTo(404);
        assertThat(noSuchPerson.errorBody().string()).isEqualTo("No such ID: 45");
    }

    @Test
    public void createWithIncompleteDataReturnsBAD_REQUEST() throws IOException {
        //create a person with ID 42
        executeCall(adminClient.createPerson(Person.builder().id("42").name("Jane").build()));
        //now try to create a new one with the same ID
        Response<Void> personExists = adminClient.createPerson(Person.builder().id("42").name("Jane").build()).execute();
        assertThat(personExists.code()).isEqualTo(400);
        assertThat(personExists.errorBody().string()).isEqualTo("Person with ID already exists: 42");
        //unhappy flow: POST with blank name
        Response<Void> incompletePost = adminClient.createPerson(Person.builder().id("44").name(null).build()).execute();
        assertThat(incompletePost.code()).isEqualTo(400);
        assertThat(incompletePost.errorBody().string()).isEqualTo("Person name cannot be null");
    }


    private <T> Response<T> executeCall(Call<T> call) throws IOException {
        Response<T> response = call.execute();
        if (!response.isSuccessful()) {
            fail("response returned " + response.errorBody().string());
        }
        return response;
    }
}
