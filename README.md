# Summary

Spring retrofit demo is an example project to demo how to expose a REST service's API through a separate jar and use that jar in another project.

It uses Springboot, [Retrofit2](https://square.github.io/retrofit/) and [Testcontainers](https://www.testcontainers.org/) 

Read the [companion blog article](https://jaspersprengers.nl/2021/06/05/user-friendly-api-publishing-and-testing-with-retrofit2/) for a detailed discussion and background.

## Requirements

You need Maven and a local Docker installation to build the project and run the integration test.

## Project structure

* The api project contains data transfer objects and service specification interfaces decorated with Retrofit annotations.
* The service project is the REST server, which depends on the api project
* The integration project only contains a single integration test which depends on the api project and a dockerized instance of the server.

## Build 

From the project root, run `mvn install`

Things to note:

The `package` phase of the `service` project creates a Docker image using the `jib-maven-plugin` and pushes it to the local repository as `spring-retrofit-test-server`.

The `test` phase of the `integration` project starts a container of the server image and queries it using Retrofit proxies. 


