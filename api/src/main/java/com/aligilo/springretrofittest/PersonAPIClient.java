package com.aligilo.springretrofittest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface PersonAPIClient {

    /**
     * Retrieves all Person DTOs in the data store
     * @param fields a comma separated list of optional fields to include in the Person DTO. When blank, all are exported
     */
    @GET("api/person")
    Call<List<Person>> getAll(@Query("fields") String fields);

    /**
     * Retrieves a single Person DTOs by its id property
     * @param fields a comma separated list of optional fields to include in the Person DTO. When blank, all are exported
     */
    @GET("api/person/{id}")
    Call<Person> getPersonById(@Path("id") String id, @Query("fields") String fields);

    /**
     * Creates a new Person in the data store. id must be unique
     */
    @POST("api/person")
    Call<Void> createPerson(@Body Person person);

    /**
     * Creates a new Person in the data store when id is unique, otherwise the existing entry is updated.
     */
    @PUT("api/person")
    Call<Void> upsertPerson(@Body Person person);

    /**
     * Removes an entry in the data store by its id property.
     */
    @DELETE("api/person/{id}")
    Call<Void> deletePerson(@Path("id") String id);
}
