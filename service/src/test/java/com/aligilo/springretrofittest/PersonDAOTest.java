package com.aligilo.springretrofittest;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class PersonDAOTest {

    PersonDAO dao = new PersonDAO();
    Person.PersonBuilder minimal = Person.builder().id("1").name("John");
    Person.PersonBuilder allFields = minimal.address("Street").email("john@gmail.com");

    @Test
    public void putWithMandatoryDataStoresObject(){
        dao.put(minimal.build());
        Optional<Person> person = dao.getById("1", null);
        assertThat(person).isPresent();
    }

    @Test
    public void putPersonWithSameIdTwiceStoresLastState(){
        dao.put(minimal.build());
        dao.put(minimal.name("Jane").build());
        Optional<Person> person = dao.getById("1", null);
        assertThat(person.get().getName()).isEqualTo("Jane");
    }

    @Test
    public void getAllStoredPersons(){
        dao.put(minimal.build());
        dao.put(minimal.id("2").build());
        assertThat(dao.getAll(null)).hasSize(2);
    }

    @Test
    public void noFilterReturnsAllFields(){
        dao.put(allFields.build());
        Person byId = dao.getById("1", null).get();
        assertThat(byId.getAddress()).isNotNull();
        assertThat(byId.getEmail()).isNotNull();
    }

    @Test
    public void emailFilterReturnsOnlyListedFields(){
        dao.put(allFields.build());
        Person byId = dao.getById("1", "email").get();
        assertThat(byId.getAddress()).isNull();
        assertThat(byId.getEmail()).isNotNull();
    }

    @Test
    public void addressFilterReturnsOnlyListedFields(){
        dao.put(allFields.build());
        Person byId = dao.getById("1", "address").get();
        assertThat(byId.getAddress()).isNotNull();
        assertThat(byId.getEmail()).isNull();
    }

    @Test
    public void putWithoutNameThrows(){
        Assertions.assertThatThrownBy(() -> dao.put(Person.builder().id("1").build()));
    }

    @Test
    public void testDeletePerson(){
        dao.put(minimal.build());
        dao.deleteById("1");
        assertThat(dao.getAll(null)).isEmpty();
    }


}
