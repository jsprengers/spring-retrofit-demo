package com.aligilo.springretrofittest;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Service
public class AuthenticatedClientFactory {

    @Value("${server.port}")
    int port;
    @Value("${password.read}")
    String readPwd;
    @Value("${password.write}")
    String writePwd;

    PersonAPIClient getClientForUser() {
        return authenticatedClient("user", readPwd);
    }

    PersonAPIClient getClientForAdmin() {
        return authenticatedClient("admin", writePwd);
    }

    private PersonAPIClient authenticatedClient(String username, String password) {
        //Springboottest runs under port 8085, as specified in src/test/resources/application.properties
        OkHttpClient okHttpClient = new OkHttpClient.Builder().authenticator(
                (route, response) -> response.request().newBuilder().header("Authorization", Credentials.basic(username, password))
                        .build()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(String.format("http://localhost:%d/", port)).
                        addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(PersonAPIClient.class);
    }


}
