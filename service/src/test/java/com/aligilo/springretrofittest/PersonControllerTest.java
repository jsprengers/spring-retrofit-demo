package com.aligilo.springretrofittest;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PersonControllerTest {

    private PersonController controller = new PersonController(new PersonDAO());

    @Test
    public void testPostOperationWhenIdExistsWillThrow(){
        Person.PersonBuilder builder = Person.builder().id("1").name("John");
        controller.createPerson(builder.build());
        assertThatThrownBy(() -> controller.createPerson(builder.build()));
    }


}
