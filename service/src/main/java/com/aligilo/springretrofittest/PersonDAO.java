package com.aligilo.springretrofittest;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonDAO {

    private Map<String, Person> cache = new HashMap<>();

    public List<Person> getAll(String fields) {
        return cache.values().stream().map(p -> filterFields(p, fields)).collect(Collectors.toList());
    }

    public Optional<Person> getById(String id, String fields) {
        return Optional.ofNullable(filterFields(cache.getOrDefault(id, null), fields));
    }

    private Person filterFields(Person person, String fieldsFilter) {
        if (!StringUtils.hasLength(fieldsFilter)) {
            return person;
        }
        Person.PersonBuilder copy = person.toBuilder();
        if (!fieldsFilter.contains("address")) {
            copy.address(null);
        }
        if (!fieldsFilter.contains("email")) {
            copy.email(null);
        }
        return copy.build();
    }

    public void put(Person person) {
        validate(person);
        cache.put(person.getId(), person);
    }

    private void validate(Person person) {
        if (person == null) {
            throw new IllegalArgumentException("Person to store cannot be null");
        }
        if (!StringUtils.hasLength(person.getId())) {
            throw new IllegalArgumentException("Person id cannot be null");
        }
        if (!StringUtils.hasLength(person.getName())) {
            throw new IllegalArgumentException("Person name cannot be null");
        }
    }

    public void deleteById(String id) {
        cache.remove(id);
    }
}
