package com.aligilo.springretrofittest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.function.Function;

@Configuration
@EnableWebSecurity
/**
 * Basic authentication has two levels of privilege: user and admin. Only admin can execute PUT,POST,DELETE methods.
 */
public class BasicAuthSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(BasicAuthSecurityConfig.class);
    @Value("${password.read:nosecret}")
    private String readPassword;
    @Value("${password.write:secret}")
    private String writePassword;

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.PUT, "/api/**").hasRole("write")
                .antMatchers(HttpMethod.POST, "/api/**").hasRole("write")
                .antMatchers(HttpMethod.DELETE, "/api/**").hasRole("write")
                .antMatchers(HttpMethod.GET, "/api/**").hasRole("read")
                .and().httpBasic();

    }

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder authentication)
            throws Exception {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> conf = authentication
                .inMemoryAuthentication();
        conf.withUser("admin").password(encoder.encode(writePassword)).roles("read", "write");
        conf.withUser("user").password(encoder.encode(readPassword)).roles("read");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
