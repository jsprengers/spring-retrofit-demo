package com.aligilo.springretrofittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRetrofitTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRetrofitTestApplication.class, args);
    }

}
