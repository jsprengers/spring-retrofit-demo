package com.aligilo.springretrofittest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/person")
@RequiredArgsConstructor
@Slf4j
public class PersonController {

    @Autowired
    private final PersonDAO personDAO;

    @GetMapping
    List<Person> getAll(@RequestParam(value = "fields", required = false) String fields) {
        return personDAO.getAll(fields);
    }

    @GetMapping("/{id}")
    Person getPersonById(@PathVariable("id") String id, @RequestParam(value = "fields", required = false) String fields) {
        return personDAO.getById(id, fields).orElseThrow(() -> {
            throw new NotFoundException("No such ID: " + id);
        });
    }

    @PostMapping
    void createPerson(@RequestBody Person person) {
        if (personDAO.getById(person.getId(), null).isPresent()) {
            throw new IllegalArgumentException("Person with ID already exists: " + person.getId());
        }
        log.info("Storing person with id {}", person.getId());
        personDAO.put(person);
    }

    @PutMapping
    void upsertPerson(@RequestBody Person person) {
        personDAO.put(person);
    }

    @DeleteMapping("/{id}")
    void deletePerson(@PathVariable("id") String id) {
        personDAO.deleteById(id);
    }

}
